/*
 * Copyright (C) 2020 EPED UNED lsi Teaching team
 * Copyright (C) 2020 Jose Antonio Cuello Principal <jcuello11@alumno.uned.es>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.uned.lsi.eped.pract2019_2020;

/**
 * EPED Pract2019_2020.
 * Base class for dictionary nodes.
 *
 * @author EPED UNED lsi Teaching team
 * @version 1.0 - 15 mar 2020
 */
public abstract class Node {
	public enum NodeType {
		ROOTNODE, LETTERNODE, WORDNODE
	}

	/* Prescribe un getter que devuelve el tipo de nodo */
	public abstract NodeType getNodeType();
}
