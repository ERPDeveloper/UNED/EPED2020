/*
 * Copyright (C) 2020 EPED UNED lsi Teaching team
 * Copyright (C) 2020 Jose Antonio Cuello Principal <jcuello11@alumno.uned.es>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.uned.lsi.eped.pract2019_2020;

import es.uned.lsi.eped.DataStructures.GTree;
import es.uned.lsi.eped.DataStructures.GTreeIF;
import es.uned.lsi.eped.DataStructures.ListIF;

/**
 * EPED Pract2019_2020.
 * Class to store the word list of a dictionary.
 *
 * @author EPED UNED lsi Teaching team
 * @author Jose Antonio Cuello Principal
 * @version 1.0 - 15 mar 2020
 */
public class Dictionary {

    /**
     * El diccionario es un arbol general de nodos.
     *
     * @var GTree
     */
	private final GTree<Node> dict;

	/**
     * Constructor de la clase.
     */
	public Dictionary() {
        this.dict = new GTree<>();
        this.dict.setRoot(new RootNode());
    }

	/**
     * Metodo de insercion de una nueva palabra en el diccionario.
     *
     * @param word
     */
	public void insert(String word) {
		insertInTree(word, this.dict);
	}

	/**
     * Metodo publico de busqueda de todas las palabras a partir de una secuencia.
     *
     * @param sequence
     * @return WordList
     */
	public WordList search(String sequence) {
        WordList salida = new WordList();
        searchInTree(sequence, "", this.dict, salida);
		return salida;
	}

	/**
     * Metodo publico de busqueda de todas las palabras de tamaño size
     * a partir de una secuencia.
     *
     * @param sequence
     * @param size
     * @return WordListN
     */
	public WordListN search(String sequence, int size) {
		WordListN salida = new WordListN(size);
		searchInTreeN(sequence, "", this.dict, salida, size);
		return salida;
	}

	/**
     * Insert a new word into dictionary.
     * (Loop Mode)
     *
     * @param word
     * @param node
     */
	private void insertInTree_while(String word, GTreeIF<Node> node) {
        GTreeIF<Node> child;
        int letterPos = 0;
        int childPos = 1;

        // Search position into tree for new nodes
        while (childPos <= node.getNumChildren() && letterPos < word.length()) {
            child = node.getChild(childPos);
            char letter = word.charAt(letterPos);
            int compare = NodeTools.checkLetterInNodeLetter(child.getRoot(), letter);
            // Case word letter is equal than actual node letter value. Down level
            if (compare == 0) {
                node = child;
                childPos = 1;
                letterPos++;
                continue;
            }

            // Case word letter is minor than actual node letter value. break while.
            if (compare == 1) {
                break;
            }

            // the node is invalid or the letter being processed is greater than that of the node
            childPos++;
        }

        // Add word into tree
        this.insertWord(word.substring(letterPos), node, childPos);
    }

	/**
     * Insert a new word into dictionary.
     * (Recursive Mode)
     *
     * @param word
     * @param node
     */
	private void insertInTree(String word, GTreeIF<Node> node) {
        GTreeIF<Node> child;
        int childPos = 1;

        while (childPos <= node.getNumChildren() && word.length() > 0) {
            child = node.getChild(childPos);
            int compare = NodeTools.checkLetterInNodeLetter(child.getRoot(), word.charAt(0));
            // Case word letter is equal than actual node letter value. Down level
            if (compare == 0) {
                this.insertInTree(word.substring(1), child);
                return;
            }

            // Case word letter is minor than actual node letter value. break while.
            if (compare == 1) {
                break;
            }

            // the node is invalid or the letter being processed is greater than that of the node
            childPos++;
        }

        // Add word into tree
        this.insertWord(word, node, childPos);
    }

    /**
     * Insert each letter of the word in a tree node.
     * Add word ending to new word.
     *
     * @param word
     * @param node
     * @param position
     */
    private void insertWord(String word, GTreeIF<Node> node, int position) {
        // If the dictionary is not in order, the children may already exist. Only mark new word.
        if (word.equals("")) {
            NodeTools.setTreeWordToNode(node);
            return;
        }

        // Insert firt character into node position.
        GTreeIF<Node> newNode = NodeTools.getTreeLetterNode(word.charAt(0));
        node.addChild(position, newNode);
        node = newNode;

        // Insert others character at first position, of new branch
        for (int x = 1; x < word.length(); x++) {
            newNode = NodeTools.getTreeLetterNode(word.charAt(x));
            node.addChild(1, newNode);
            node = newNode;
        }

        // Add word ending to the branch
        NodeTools.setTreeWordToNode(node);
    }

    /**
     * Remove first occurrence of the character at the indicated from the string.
     *
     * @param text
     * @param letter
     * @return String
     */
    private String removeLetter(String text, char letter) {
        int position = text.indexOf(letter);
        if (position < 0) {
            return text;
        }

        StringBuilder sb = new StringBuilder(text);
        sb.deleteCharAt(position);
        return sb.toString();
    }

	/**
     * Busqueda de todas las palabras a partir de una secuencia.
     *
     * @param sequence
     * @param word
     * @param node
     * @param salida
     */
	private void searchInTree(String sequence, String word, GTreeIF<Node> node, WordList salida) {
        // We process all the children of the node in search of words.
        ListIF <GTreeIF<Node>> children = node.getChildren();
        for (int posChild = 1; posChild <= children.size(); posChild++) {
            GTreeIF<Node> child = children.get(posChild);

            // Check if it is a word ending. If it is, add to word list.
            if (child.getRoot().getNodeType() == Node.NodeType.WORDNODE) {
                salida.add(word);
                continue; // to next children node
            }

            // Check if there are letters in the sequence.
            if (sequence.isEmpty()) {
                break;
            }

            // It is a letter node. Check if the letter is in the sequence.
            LetterNode letterNode = (LetterNode) child.getRoot();
            char letter = letterNode.getLetter();
            if (sequence.indexOf(letter) < 0) {
                continue; // to next children node
            }

            // Add letter to word and down into child tree.
            String newSequence = this.removeLetter(sequence, letter);
            String newWord = word + letter;
            this.searchInTree(newSequence, newWord, child, salida);
        }
    }

	/**
     * Busqueda de todas las palabras de tamaño size a partir de una secuencia.
     *
     * @param sequence
     * @param word
     * @param node
     * @param salida
     * @param size
     */
	private void searchInTreeN(String sequence, String word, GTreeIF<Node> node, WordListN salida, int size) {
        // We process all the children of the node in search of words.
        ListIF <GTreeIF<Node>> children = node.getChildren();
        for (int posChild = 1; posChild <= children.size(); posChild++) {
            GTreeIF<Node> child = children.get(posChild);

            // Check if it is a word ending. If it is, add to word list.
            if (child.getRoot().getNodeType() == Node.NodeType.WORDNODE) {
                if (word.length() == size) {
                    salida.add(word);
                    break;
                }
                continue; // to next children node
            }

            // Check if there are letters in the sequence and if word can grow.
            if (sequence.isEmpty() || word.length() == size) {
                break;
            }

            // It is a letter node. Check if the letter is in the sequence.
            LetterNode letterNode = (LetterNode) child.getRoot();
            char letter = letterNode.getLetter();
            if (sequence.indexOf(letter) < 0) {
                continue; // to next children node
            }

            // Add letter to word and down into child tree.
            String newSequence = this.removeLetter(sequence, letter);
            String newWord = word + letter;
            this.searchInTreeN(newSequence, newWord, child, salida, size);
        }
    }
}
