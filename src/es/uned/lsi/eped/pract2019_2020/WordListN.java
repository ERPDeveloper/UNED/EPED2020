/*
 * Copyright (C) 2020 EPED UNED lsi Teaching team
 * Copyright (C) 2020 Jose Antonio Cuello Principal <jcuello11@alumno.uned.es>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.uned.lsi.eped.pract2019_2020;

import es.uned.lsi.eped.DataStructures.Queue;
import es.uned.lsi.eped.DataStructures.QueueIF;

/**
 * EPED Pract2019_2020.
 * Class to store a list of words arranged alphabetically and of the same length.
 *
 * @author EPED UNED lsi Teaching team
 * @author Jose Antonio Cuello Principal
 * @version 1.0 - 15 mar 2020
 */
public class WordListN {

    /**
     * Contain word list.
     *
     * @var List
     */
    private final QueueIF<String> words;

    /**
     * Indicates the length of the words.
     *
     * @var int
     */
	private final int wordSize;

    /**
     * Class constructor.
     *
     * @param size
     */
	public WordListN(int size) {
        this.words = new Queue<>();
        this.wordSize = size;
    }

    /**
     * Add the indicated word to the word list.
     *
     * @param word
     */
	public void add(String word) {
        this.words.enqueue(word);
    }

    /**
     * Get the length of the words.
     *
     * @return int
     */
	public int getWordSize() {
        return this.wordSize;
    }

    /**
     * Displays the list of words on the screen.
     *
     * @return String
     */
    @Override
	public String toString() {
		StringBuilder display = new StringBuilder();
		display.append(this.getHeaderText());

        int size = this.words.size();
		for (int pos = 1; pos <= size; pos++) {
			display.append(this.getWord(false));
			if ( pos < size ) {
				display.append(", ");
			}
		}
		display.append('\n');
		return display.toString();
	}

    /**
     * Get Header message.
     *
     * @return String
     */
    private String getHeaderText() {
        String text = "-Palabras de " + this.getWordSize() + " letra";
		if ( this.getWordSize() > 1 ) {
            text += "s";
        }
		text +=": ";

        return text;
    }

    /**
     * Get current queue word
     *
     * @return String
     */
    private String getWord(boolean destroy) {
        String word = (String) this.words.getFirst();
        this.words.dequeue();
        if (!destroy) {
            this.words.enqueue(word);
        }
        return word;
    }
}
