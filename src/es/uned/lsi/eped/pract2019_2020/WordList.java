/*
 * Copyright (C) 2020 EPED UNED lsi Teaching team
 * Copyright (C) 2020 Jose Antonio Cuello Principal <jcuello11@alumno.uned.es>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.uned.lsi.eped.pract2019_2020;

import es.uned.lsi.eped.DataStructures.List;
import es.uned.lsi.eped.DataStructures.ListIF;

/**
 * EPED Pract2019_2020.
 * Class to store a list of words arranged alphabetically.
 *
 * @author EPED UNED lsi Teaching team
 * @author Jose Antonio Cuello Principal
 * @version 1.0 - 15 mar 2020
 */
public class WordList {

    /**
     * Contain list of word list.
     *
     * @var WordListN
     */
	private final ListIF<WordListN> wordList;

    /**
     * Class constructor.
     */
	public WordList() {
		this.wordList = new List<>();
	}

    /**
     * Add the indicated word to the correct word list based on its length.
     * Look for the list of words with the same length.
     * If it does not exist, create it.
     *
     * @param word
     */
	public void add(String word) {
        WordListN list = null;
        int length = word.length();
        int size = this.wordList.size();
        int position = 1;

        while (position <= size) {
            list = this.wordList.get(position);
            if (length <= list.getWordSize()) {
                break;
            }
            position++;
        }

        if (list == null || length != list.getWordSize()) {
            list = new WordListN(length);
            this.wordList.insert(position, list);
        }
        list.add(word);
    }

    /**
     * Displays the list of words on the screen.
     *
     * @return String
     */
    @Override
	public String toString() {
		StringBuilder display = new StringBuilder();
		for ( int pos = this.wordList.size(); pos >= 1; pos-- ) {
			display.append(this.wordList.get(pos).toString());
		}
		return display.toString();
	}
}
