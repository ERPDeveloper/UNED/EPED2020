/*
 * Copyright (C) 2020 EPED UNED lsi Teaching team
 * Copyright (C) 2020 Jose Antonio Cuello Principal <jcuello11@alumno.uned.es>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.uned.lsi.eped.pract2019_2020;

import es.uned.lsi.eped.DataStructures.GTree;
import es.uned.lsi.eped.DataStructures.GTreeIF;

/**
 * EPED Pract2019_2020.
 * Class to facilitate tasks with the Node class.
 *
 * @author Jose Antonio Cuello Principal
 * @version 1.0 - 15 mar 2020
 */
public class NodeTools {
    /**
     * Check if node is a LetterNode and has same param letter.
     * Return value:
     *   0: param letter is equal than letter node value
     *   1: param letter is minor then letter node value
     *  -1: param letter is greater then letter node value or node not is a LetterNode
     *
     * @param node
     * @param letter
     * @return int
     */
    public static int checkLetterInNodeLetter(Node node, char letter) {
        if (node.getNodeType() == Node.NodeType.LETTERNODE) {
            LetterNode letterNode = (LetterNode) node;
            return letterNode.compareTo(letter);
        }
        return -1;
    }

    /**
     * Create a new tree node and assign it a letter.
     *
     * @param letter
     * @return GTree
     */
    public static GTreeIF<Node> getTreeLetterNode(char letter) {
        GTreeIF<Node> newNode = new GTree<>();
        newNode.setRoot(new LetterNode(letter));
        return newNode;
    }

    /**
     * Add a word ending mark to indicate node.
     *
     * @param node
     */
    public static void setTreeWordToNode(GTreeIF<Node> node) {
        GTreeIF<Node> newNode = new GTree<>();
        newNode.setRoot(new WordNode());
        node.addChild(1, newNode);
    }
}

