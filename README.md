# EPED 2019-2020

Creación de un diccionario de palabras en español y búsqueda de palabras en base a un conjunto de letras.

Concretamente el programa recibirá una serie de letras y nos devolverá todas las palabras válidas que 
se puedan construir con ellas. Para comprobar si una palabra es válida o no, es imprescindible el
uso de un diccionario.


### Pre-requisitos

Desarrollado en Java con el JDK 12, Encoding UTF-8


## Ejecutando las pruebas

Para la ejecución del programa se deberá abrir una consola y ejecutar:

    `java -jar eped2020.jar <diccionario> <busquedas>`


siendo:

• <diccionario> nombre del fichero con las palabras a insertar en el diccionario.
• <busquedas> nombre del fichero con las búsquedas a realizar.


## Autores

* **EPED UNED (lsi)** - *Interfaces e Implementación inicial*
* **José Antonio Cuello** - *Implementación final*

## Licencia

Este proyecto está bajo la Licencia (GNU General Public License v3.0) - mira el archivo [LICENSE.md](LICENSE.md) para detalles
